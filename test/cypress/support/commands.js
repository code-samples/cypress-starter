import {url} from '../fixtures/default-values'

Cypress.Commands.add("openMapAt", (lat=url.lat, lng=url.lng, zoom=url.zoom) => {
  cy.visit(`/?lat=${lat}&lng=${lng}&zoom=${zoom}`);
});

Cypress.Commands.add("getVisibleMarkers", () => {
  cy.get('.leaflet-marker-icon').filter(':visible');
});

Cypress.Commands.add("getBySel", (selector, ...args) => {
  return cy.get(`[data-test-name=${selector}]`, ...args);
});

Cypress.Commands.add("getBySelLike", (selector, ...args) => {
  return cy.get(`[data-test-name*=${selector}]`, ...args);
});

Cypress.Commands.add('clickLink', (label) => {
  cy.get('a').contains(label).click()
})

Cypress.Commands.add('openLocation', (label) => {
  cy.get('a').contains(label).click()
})
