import dotenv from 'dotenv'

dotenv.config({ path: ".env.local" });
dotenv.config();

import "./commands";


after(() => {
  // cy.task('generateReport')
})
